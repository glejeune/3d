$fn=180;

border_depth=3;
border_width=200;
border_height=60;

roller_bearing_width=7;
roller_bearing_inner_radius=4;
roller_bearing_outter_radius=6;

filament_roll_depth=61;

floor_width = filament_roll_depth+roller_bearing_width+2;

module border() {
    rotate([90, 0, 90])
    difference() {
        union() {
            resize([border_width,border_height,border_depth]) 
                linear_extrude(height = border_depth, center = false, convexity = 10) 
                import(file="shape.dxf", layer="shape");
            #translate([20, 45, 0]) 
                cylinder(h=border_depth+1, r=roller_bearing_outter_radius);
            #translate([180, 45, 0]) 
                cylinder(h=border_depth+1, r=roller_bearing_outter_radius);
        }
        #translate([20, 45, 0]) cylinder(h=border_depth+1, r=roller_bearing_inner_radius);
        #translate([180, 45, 0]) cylinder(h=border_depth+1, r=roller_bearing_inner_radius);
    }
}

union() {
    border();
    translate([border_depth*2+roller_bearing_width+2, border_width, 0]) rotate([0, 0, 180]) border();

    difference() {
        translate([border_depth, 0, 0])
            cube([floor_width, border_width, border_depth*2]);
        translate([border_depth, border_depth*2, 0])
            cube([floor_width, border_width-border_depth*4, border_depth*2]);
    }
    
    translate([floor_width+2*border_depth, border_width, 0]) rotate([0, 0, 180]) border();
    translate([floor_width-roller_bearing_width-2, 0, 0]) border();
}