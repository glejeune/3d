include <BOSL2/std.scad>
include <BOSL2/gears.scad>

$fn=180;

module screw_pass() {
  rotate([0, 90, 0]) translate([-5.25, 0, 6.5])  cylinder(h=6, d=2, center=true); 
  rotate([0, 90, 0]) translate([-5.25, 0, 6.25]) cylinder(h=1.6, d=4.5, center=true, $fn=6);
  rotate([0, 90, 0]) translate([-7.3, 0, 6.25]) cube([3.9, 3.9, 1.6], center=true);
}

difference() {
  union() {
    spur_gear(pitch=5, teeth=25, thickness=6, shaft_diam=0);
    translate([0, 0, 5]) cylinder(h=5, d=18, center=true);
    translate([0, 0, -3.5]) cylinder(h=1, d=18, center=true);
  }
  translate([0, 0, 1.7]) cylinder(h=13, d=8, center=true);

  screw_pass();
  rotate([0, 0, 120]) screw_pass();
  rotate([0, 0, 240]) screw_pass();
}
