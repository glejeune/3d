include <BOSL2/std.scad>
include <BOSL2/screws.scad>

$fn=90;

motor_width=42.3;
motor_screw_distance=31;
motor_screw_diameter=3;
motor_axe_diameter=5+1;
motor_axe_sep_diameter=22;
motor_axe_dep_depth=2;

tolerance=1;

plate_depth=5;
plate_width=motor_width + plate_depth*2 + 2;

border_support_distance = sqrt(2 * plate_depth^2);

module border() {
    translate([plate_depth/2, 0, 0])
        rotate([90, 0, 270])
        difference() {
            linear_extrude(plate_depth, center=true)
                polygon(points=[[0, 0], [plate_width, 0], [0, plate_width]]);
            linear_extrude(plate_depth+tolerance, center=true)
                polygon(points=[[0, 0], [plate_width-border_support_distance, 0], [0, plate_width-border_support_distance]]);
        }
}

union() {
    difference() {
        cube([plate_width, plate_depth, plate_width]);
        
        // Axe hole
        translate([plate_width/2, plate_depth/2, plate_width/2]) 
            rotate([90, 0, 0])
            cylinder(h=plate_depth+tolerance, d=motor_axe_diameter, center=true);
        
        // Axe support hole
        translate([plate_width/2, (motor_axe_dep_depth-tolerance)/2, plate_width/2])
            rotate([90, 0, 0])
            cylinder(h=motor_axe_dep_depth+tolerance, d=motor_axe_sep_diameter, center=true);
        
        // Screw holes
        translate([(plate_width-motor_screw_distance)/2, plate_depth/2, (plate_width-motor_screw_distance)/2])
            rotate([90, 0, 0])
            cylinder(h=plate_depth+tolerance, d=motor_screw_diameter, center=true);
        translate([(plate_width-motor_screw_distance)/2 + motor_screw_distance, plate_depth/2, (plate_width-motor_screw_distance)/2])
            rotate([90, 0, 0])
            cylinder(h=plate_depth+tolerance, d=motor_screw_diameter, center=true);
        translate([(plate_width-motor_screw_distance)/2 + motor_screw_distance, plate_depth/2, (plate_width-motor_screw_distance)/2 + motor_screw_distance])
            rotate([90, 0, 0])
            cylinder(h=plate_depth+tolerance, d=motor_screw_diameter, center=true);
        translate([(plate_width-motor_screw_distance)/2, plate_depth/2, (plate_width-motor_screw_distance)/2 + motor_screw_distance])
            rotate([90, 0, 0])
            cylinder(h=plate_depth+tolerance, d=motor_screw_diameter, center=true);

    }
    
    difference() {
        translate([0, 0, -plate_depth])rotate([90, 0, 0])cube([plate_width, plate_depth, plate_width]);
        #translate([(plate_width-motor_screw_distance)/2, -(plate_width-motor_screw_distance)/2, 0])
          screw_hole("M2x1", length=plate_depth, anchor=TOP,  head="flat");
        #translate([(plate_width-motor_screw_distance)/2 + motor_screw_distance, -(plate_width-motor_screw_distance)/2, 0])
          screw_hole("M2x1", length=plate_depth, anchor=TOP,  head="flat");
        #translate([(plate_width-motor_screw_distance)/2, -(plate_width-motor_screw_distance)/2 - motor_screw_distance, 0])
          screw_hole("M2x1", length=plate_depth, anchor=TOP,  head="flat");
        #translate([(plate_width-motor_screw_distance)/2 + motor_screw_distance, -(plate_width-motor_screw_distance)/2 - motor_screw_distance, 0])
          screw_hole("M2x1", length=plate_depth, anchor=TOP,  head="flat");
    }
    rotate([270, 0, 0]) cube([plate_width, plate_depth, plate_depth]);
    
    border();
    translate([plate_width-plate_depth, 0, 0]) border();
}


