include <common-check.scad>

$fn=90;

drawerHandleDiameter=min(i_drawerInnerHeight, i_drawerInnerWidth);
totalDrawerWidth = i_drawerInnerWidth*i_nbParts+(i_nbParts-1)*i_drawerInnerBorder;
totalDrawerDepth = i_drawerInnerDepth;
totalDrawerHeight = i_drawerInnerHeight;

union() {
  difference() {
    minkowski() {
        cube([totalDrawerWidth, totalDrawerDepth, totalDrawerHeight]);
        translate([i_drawerExternalBorder, i_drawerExternalBorder, 0]) cylinder(r=i_drawerExternalBorder,h=2);
    }
    
    for(n = [1:i_nbParts]) {
        translate([i_drawerExternalBorder+(n-1)*(i_drawerInnerBorder+i_drawerInnerWidth), i_drawerExternalBorder, i_drawerExternalBorder]) cube([i_drawerInnerWidth, i_drawerInnerDepth, i_drawerInnerHeight+1]);
    }
  }
  color("blue") difference() {
    rotate([90,0,0]) translate([(totalDrawerWidth+2*i_drawerExternalBorder)/2, totalDrawerHeight/2+i_drawerExternalBorder/2 - drawerHandleDiameter/4, 0]) cylinder(h=i_handleHeight, d=drawerHandleDiameter, $fn=10);
    #rotate([90,0,0]) translate([(totalDrawerWidth+2*i_drawerExternalBorder)/2, totalDrawerHeight/2+i_drawerExternalBorder/2 - drawerHandleDiameter/4, 0]) cylinder(h=i_handleHeight-i_drawerInnerBorder, d=drawerHandleDiameter-2*i_drawerInnerBorder, $fn=10);
    #rotate([90,0,0]) translate([(totalDrawerWidth+2*i_drawerExternalBorder)/2 - drawerHandleDiameter/2, totalDrawerHeight/2+i_drawerExternalBorder/2 - 3*drawerHandleDiameter/4, 0]) cube([drawerHandleDiameter, drawerHandleDiameter/2, 5]);
  }
}
