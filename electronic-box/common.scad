// You will prbably want to change this :
nbDrawersHeight = 3;
nbDrawersWidth = 1;
withStoppers = true;

// You can also change that (with caution) :
nbParts=3;
boxTolerance=1;

// But if you change something bellow, you know what you are doing ;)
boxExternalBorder=2;
boxHolderSize=6;
boxStopperHeight=1;

handleHeight=5;

drawerExternalBorder=2;
drawerInnerBorder=1;
drawerInnerWidth=30;
drawerInnerDepth=67;
drawerInnerHeight=22;

boxStopperDepth = 1;

decorationBorderDepth = 22;
nbDecorations = 6;
