include <common-check.scad>

$fn=90;

totalDrawerWidth = i_boxTolerance + i_drawerInnerWidth*i_nbParts+(i_nbParts-1)*i_drawerInnerBorder+2*i_drawerExternalBorder;
totalDrawerDepth = i_boxTolerance + i_drawerInnerDepth+2*i_drawerExternalBorder;
totalDrawerHeight = i_boxTolerance + i_drawerInnerHeight+i_drawerExternalBorder;

totalBoxWidth = totalDrawerWidth * i_nbDrawersWidth + i_drawerInnerBorder * (i_nbDrawersWidth - 1);
totalBoxHeight = totalDrawerHeight * i_nbDrawersHeight + i_drawerInnerBorder * (i_nbDrawersHeight - 1);

holderWidth = totalDrawerWidth - 2 * i_boxHolderSize;
holderHeight = totalDrawerHeight * i_nbDrawersHeight + i_drawerInnerBorder * (i_nbDrawersHeight - 1);
holderDepth = totalDrawerDepth - i_boxHolderSize;

module stopper() {
  s = sqrt(i_boxStopperHeight^2 + (i_boxStopperDepth)^2);
  a = atan(i_boxStopperHeight/i_boxStopperDepth);
  rotate([180,0,0]) translate([0, -i_boxStopperDepth, -i_boxExternalBorder + 0*i_boxStopperHeight ]) difference() {
    cube([i_boxHolderSize - i_drawerExternalBorder - (i_boxTolerance/2), i_boxStopperDepth, i_boxStopperHeight]);
    rotate([a, 0, 0]) cube([i_boxHolderSize - i_drawerExternalBorder - (i_boxTolerance/2), s, i_boxStopperHeight]);
  }
}

decorWidth = (totalDrawerDepth+i_boxExternalBorder)/(i_nbDecorations*2 + 1);
decorHeight = totalBoxHeight - 2*decorWidth;
module borderDecoration() {
  translate([0, 0, decorWidth/2]) union() {
    cube([i_decorationBorderDepth, decorWidth, decorHeight]);
    translate([0, decorWidth/2, 0]) rotate([0, 90, 0]) cylinder(d=decorWidth, h=i_decorationBorderDepth);
    translate([0, decorWidth/2, decorHeight]) rotate([0, 90, 0]) cylinder(d=decorWidth, h=i_decorationBorderDepth);
  }
}

union() {
  difference() {
    translate([i_boxExternalBorder, i_boxExternalBorder, i_boxExternalBorder]) minkowski() {
      cube([totalBoxWidth, totalDrawerDepth-i_boxExternalBorder, totalBoxHeight]);
      sphere(r=i_boxExternalBorder);
    }
    for (w=[1:i_nbDrawersWidth]) {
      for (h=[1:i_nbDrawersHeight]) {
        translate([
            i_boxExternalBorder + (w-1)*(totalDrawerWidth+i_drawerInnerBorder), 
            0, 
            i_boxExternalBorder + (h-1)*(totalDrawerHeight+i_drawerInnerBorder)
        ]) cube([totalDrawerWidth, totalDrawerDepth, totalDrawerHeight]);
      }
      translate([
          i_boxExternalBorder + i_boxHolderSize + (w-1)*(totalDrawerWidth+i_drawerInnerBorder),
          0, 
          i_boxExternalBorder,
      ]) cube([holderWidth, holderDepth, holderHeight]);
    }

    if (i_nbDecorations > 1) {
      for (d=[1:i_nbDecorations]) {
        #translate([-0.01, decorWidth * (2*d-1), decorWidth]) borderDecoration();
        #translate([totalBoxWidth+i_boxExternalBorder*2-i_decorationBorderDepth+0.01, decorWidth * (2*d-1), decorWidth]) borderDecoration();
      }
    }
  }

  if (withStoppers) {
    for (w=[1:i_nbDrawersWidth]) {
      for (h=[1:i_nbDrawersHeight]) {
        translate([
            i_boxExternalBorder + i_drawerExternalBorder + (i_boxTolerance/2) + (totalDrawerWidth+i_drawerInnerBorder)*(w-1), 
            i_drawerExternalBorder, 
            i_boxExternalBorder+i_boxTolerance+i_drawerInnerHeight + (i_drawerInnerHeight+i_drawerExternalBorder+i_drawerInnerBorder+i_boxTolerance)*(h-1)
        ]) stopper();
        translate([
            totalDrawerWidth - (i_drawerInnerBorder + i_drawerExternalBorder) + (totalDrawerWidth+i_drawerInnerBorder)*(w-1), 
            i_drawerExternalBorder, 
            i_boxExternalBorder+i_boxTolerance+i_drawerInnerHeight + (i_drawerInnerHeight+i_drawerExternalBorder+i_drawerInnerBorder+i_boxTolerance)*(h-1)
        ])stopper();
      }
    }
  }
}
