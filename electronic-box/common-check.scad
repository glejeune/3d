include <common.scad>

i_nbDrawersHeight = max(nbDrawersHeight, 1.0);
i_nbDrawersWidth = max(nbDrawersWidth, 1);
i_nbParts = max(nbParts, 1);
i_boxTolerance = max(boxTolerance, 0);
i_boxExternalBorder = max(boxExternalBorder, 1);
i_boxHolderSize = max(boxHolderSize, 2);
i_boxStopperHeight = max(boxStopperHeight, 0);
i_handleHeight = max(handleHeight, 3);
i_drawerExternalBorder = max(drawerExternalBorder, 1);
i_drawerInnerBorder = max(drawerInnerBorder, 1);
i_drawerInnerWidth = max(drawerInnerWidth, 30);
i_drawerInnerDepth = max(drawerInnerDepth, 67);
i_drawerInnerHeight = max(drawerInnerHeight, 22);
i_boxStopperDepth = max(boxStopperDepth, 1);

t_decorationBorderDepth = max(decorationBorderDepth, 0);
i_decorationBorderDepth = min(t_decorationBorderDepth, i_boxExternalBorder/3);
i_nbDecorations = max(nbDecorations, 0);

