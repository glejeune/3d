$fn=60;
// You only need to change this ;)
nbHole = 4;

length = 6 * nbHole -1;
difference() {
    translate([2,2,2])minkowski() {
        cube([length, 8, 8]);
        sphere(2);
    }
    for(n = [1:nbHole]) {
        translate([5.5 + (n-1)*6, 6, -1]) cylinder(h = 14, r = 3);
    }
    
    translate([5.5,4.5,-1])cube([length, 3, 14]);
    
    if (nbHole <= 6) { 
        translate([length/2+3, 9.5, 6]) rotate(90, [1, 0, 0]) cylinder(h = 10.5, r=3);
        translate([length/2+3, 13, 6]) rotate(90, [1, 0, 0]) cylinder(h = 14, r=1.5);
    } else {
        translate([12 + (nbHole - 7)*3, 9.5, 6]) rotate(90, [1, 0, 0]) cylinder(h = 10.5, r=3);
        translate([12 + (nbHole - 7)*3, 13, 6]) rotate(90, [1, 0, 0]) cylinder(h = 14, r=1.5);
        
        translate([length - 9 - (nbHole - 7)*2.5, 9.5, 6]) rotate(90, [1, 0, 0]) cylinder(h = 10.5, r=3);
        translate([length - 9 - (nbHole - 7)*2.5, 13, 6]) rotate(90, [1, 0, 0]) cylinder(h = 14, r=1.5);
    }
}